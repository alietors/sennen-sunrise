# Sennen sunrise

## Config
You can configure the number of coordinates generated on the ```.env``` file modifying ```DATA_SIZE``` param.

## Getting started
Run ```npm install``` to install the dependencies
## Executing
Run ```npm start``` to run the script

## Testing
Run ```npm test``` to run the test suite

## CI pipeline
Basic Gitlab CI pipeline [https://gitlab.com/alietors/sennen-sunrise/pipelines](https://gitlab.com/alietors/sennen-sunrise/pipelines)
