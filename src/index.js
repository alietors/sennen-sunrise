require('dotenv').config();
const getRandomCoordinates = require('./coordinatesProvider');
const SunriseService = require('./SunriseService');
const SunriseApi = require('./SunriseApi');

const dataSize = process.env.DATA_SIZE;
const endpoint = process.env.SUNRISE_ENDPOINT;

const coordinates = getRandomCoordinates(dataSize);
const sunriseApi = new SunriseApi(endpoint);
const sunriseService = new SunriseService(sunriseApi);

(async () => {
    const info = await sunriseService.collectInfo(coordinates);
    const earlier = sunriseService.getEarlierSunrise(info);

    console.log(`Sunrise at: ${earlier.sunrise}`);
    console.log(`Day length: ${earlier.day_length}`);
})();
