// Generate a number between -180 and 180 with 4 decimal points
function getRandomCoordinate() {
    return (Math.random() * 360 - 180).toFixed(4) * 1;
}

/*
params:
    - num: Number of coordinates pairs to generate
return:
    Array of random generates coordinates
 */
function getRandomCoordinates(num) {
    const coordinatesList = [];

    for (let i = 0; i < num; i++) {
        const coordinates = [getRandomCoordinate(), getRandomCoordinate()];
        coordinatesList.push(coordinates);
    }
    return coordinatesList;
}

module.exports = getRandomCoordinates;
