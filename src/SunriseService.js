class SunriseService {
    /*
        params:
            - sunriseApi: Api used to get the sunrise information
     */
    constructor(sunriseApi) {
        this.sunriseApi = sunriseApi;
    }

    /*
    params:
        - sunriseInfo: Array of sunrise information
    return: Earlier sunrise information
     */
    getEarlierSunrise(sunriseInfo) {
        function compare(a, b) {
            if (a.sunrise < b.sunrise) {
                return -1;
            } if (a.sunrise > b.sunrise) {
                return 1;
            }
            return 0;
        }

        sunriseInfo.sort(compare);

        return sunriseInfo[0];
    }

    /*
    Collect sunrise information. It run 5 parallel calls to the api every 5 seconds
    params:
        - coordinates: Array of coordinates pairs to retrieve information
    return: sunrise information for each coordinates
     */
    async collectInfo(coordinates) {
        const sunriseInfo = [];
        let i = 0;
        //Size of the last batch if the number of coordinates is not multiple of 5
        const last = coordinates.length % 5;
        //Number of batches to process
        const batches = Math.floor(coordinates.length / 5);
        //When the last batch start
        const lastBatch = batches * 5;

        return new Promise((resolve) => {
            const addResults = async () => {
                //If we are on the last batch we only process what is left
                const size = i === lastBatch ? last : 5;
                const results = await this._partialInfo(coordinates, i, size);
                sunriseInfo.push(...results);
                i += 5;
                if (i < coordinates.length) {
                    setTimeout(addResults, 5000);
                } else {
                    resolve(sunriseInfo);
                }
            };
            addResults();
        });
    }

    async _partialInfo(coordinates, i, size) {
        const batch = [];

        for (let x = 0; x < size; x++) {
            batch.push(this.sunriseApi.getSunriseInformation(coordinates[i + x]));
        }

        return Promise.all(batch);
    }
}

module.exports = SunriseService;
