const fetch = require('node-fetch');

class SunriseApi {
    /*
    params:
        endpoint: Endpoint of the sunrise api
     */
    constructor(endpoint) {
        this.endpoint = endpoint;
    }

    /*
    params:
        - coordinate: Array with 2 values latitude and longitude
    return: Results of the api call + the coordinates
     */
    async getSunriseInformation(coordinate) {
        const url = `${this.endpoint}?lat=${coordinate[0]}&lng=${coordinate[1]}`;

        try {
            const response = await fetch(url);
            if (response.ok) {
                const jsonResponse = await response.json();
                let results = jsonResponse.results;
                results.coordinates = coordinate;
                return results;
            } else {
                return [];
            }
        } catch (error) {
            console.log(error);
            return [];
        }
    }
}

module.exports = SunriseApi;
