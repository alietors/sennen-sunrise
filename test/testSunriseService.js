var chai = require("chai");
var chaiAsPromised = require("chai-as-promised");

chai.use(chaiAsPromised);
chai.should();
var assert = chai.assert;

const SunriseService = require('../src/SunriseService');
const SunriseApi = require('../src/SunriseApi');

const endpoint = 'https://api.sunrise-sunset.org/json';
const sunriseApi = new SunriseApi(endpoint);
const sunriseService = new SunriseService(sunriseApi);

describe('SunriseService', function() {
    describe('getEarlierSunrise', function() {
        it('should order sunrise info ascending', function() {
            const sunriseInfoList = [
                {
                    sunrise: '8:42:33 AM',
                    sunset: '8:48:35 PM',
                    solar_noon: '2:45:34 PM',
                    day_length: '12:06:02',
                    civil_twilight_begin: '9:21:06 AM',
                    civil_twilight_end: '8:10:03 PM',
                    nautical_twilight_begin: '10:06:54 AM',
                    nautical_twilight_end: '7:24:14 PM',
                    astronomical_twilight_begin: '10:55:19 AM',
                    astronomical_twilight_end: '6:35:50 PM'
                },
                {
                    sunrise: '2:11:42 AM',
                    sunset: '1:55:48 PM',
                    solar_noon: '8:03:45 AM',
                    day_length: '11:44:06',
                    civil_twilight_begin: '2:36:50 AM',
                    civil_twilight_end: '1:30:40 PM',
                    nautical_twilight_begin: '3:06:20 AM',
                    nautical_twilight_end: '1:01:10 PM',
                    astronomical_twilight_begin: '3:36:15 AM',
                    astronomical_twilight_end: '12:31:15 PM'
                },
            ];
            const earlier = sunriseService.getEarlierSunrise(sunriseInfoList);
            assert.equal(earlier.sunrise, '2:11:42 AM');
        });
    });
    describe('collectInfo', function() {
        it('should return sunrise information', async function() {
            const coordinates = [ [ 92.4493, 76.5013 ], [ 114.2393, -142.7943 ] ];

            return sunriseService.collectInfo(coordinates).should.eventually.have.length(2);
        });
    });
});
