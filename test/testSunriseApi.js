var chai = require("chai");
var chaiAsPromised = require("chai-as-promised");

chai.use(chaiAsPromised);
chai.should();
var assert = chai.assert;
var expect = chai.expect;

const SunriseApi = require('../src/SunriseApi');

const endpoint = 'https://api.sunrise-sunset.org/json';
const sunriseApi = new SunriseApi(endpoint);

describe('SunriseApi', function() {
    describe('getSunriseInformation', function() {
        it('should fulfill', function() {
            const coordinates = [ 92.449, 76.501 ];
            const info = sunriseApi.getSunriseInformation(coordinates);
            assert.isFulfilled(info);
        });
        it('should return sunrise information', function() {
            const coordinates = [ 92.449, 76.501 ];
            const info = sunriseApi.getSunriseInformation(coordinates);
            return expect(info).to.eventually.have.property('sunrise');
        });
    });
});
