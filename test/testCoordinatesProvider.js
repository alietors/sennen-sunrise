var chai = require("chai");
var chaiAsPromised = require("chai-as-promised");
var assert = chai.assert;

const coordinatesProvider = require('../src/coordinatesProvider');

describe('CoordinatesProvider', function() {
    describe('getRandomCoordinates', function() {
        it('should return 10 coordinates if asked for 10', function() {
            const coordinates = coordinatesProvider(10);
            assert.equal(coordinates.length, 10);
        });

        it('coordinates should contain 2 points', function() {
            const coordinates = coordinatesProvider(1);
            assert.equal(coordinates[0].length, 2);
        });

        it('if negative number of coordinates required return empty ', function() {
            const coordinates = coordinatesProvider(-1);
            assert.equal(coordinates.length, 0);
        });
    });
});
